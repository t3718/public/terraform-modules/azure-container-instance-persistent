output "ip_address" {
  value       = "${azurerm_container_group.aci_iac_workflow.ip_address}:${var.pers_container_port}"
  description = "the ip address of the container group if dns_name_label is set"
}

output "fqdn" {
  value       = "http://${azurerm_container_group.aci_iac_workflow.fqdn}:${var.pers_container_port}"
  description = "the dns fqdn of the container group if dns_name_label is set"
}