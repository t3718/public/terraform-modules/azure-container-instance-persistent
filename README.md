[![pipeline status](https://gitlab.com/t3718/public/terraform-modules/azure-container-instance-persistent/badges/main/pipeline.svg)](https://gitlab.com/t3718/public/terraform-modules/azure-container-instance-persistent/-/commits/main) 

<!-- BEGIN_TF_DOCS -->

## Requirements

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | =2.46.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | =2.46.0 |


## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_container_group.aci_iac_workflow](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/container_group) | resource |
| [azurerm_storage_account.sa_iac_workflow](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_account) | resource |
| [azurerm_storage_share.ss_iac_workflow](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_share) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_azurerm_resource_group_location"></a> [azurerm\_resource\_group\_location](#input\_azurerm\_resource\_group\_location) | The Azure location where all resources in this module should be created | `string` | `"location"` | no |
| <a name="input_azurerm_resource_group_name"></a> [azurerm\_resource\_group\_name](#input\_azurerm\_resource\_group\_name) | The Azure resource group where all resources in this module should be created | `string` | `"iac-rg"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name | `string` | `"int"` | no |
| <a name="input_pers_app_name"></a> [pers\_app\_name](#input\_pers\_app\_name) | The application name | `string` | `"aci-example"` | no |
| <a name="input_pers_container_cpu"></a> [pers\_container\_cpu](#input\_pers\_container\_cpu) | The docker container cpu | `string` | `"0.5"` | no |
| <a name="input_pers_container_image"></a> [pers\_container\_image](#input\_pers\_container\_image) | The docker container image | `string` | `"lscr.io/linuxserver/webtop:ubuntu-xfce"` | no |
| <a name="input_pers_container_memory"></a> [pers\_container\_memory](#input\_pers\_container\_memory) | The docker container memory (RAM) | `string` | `"1.5"` | no |
| <a name="input_pers_container_name"></a> [pers\_container\_name](#input\_pers\_container\_name) | The docker container name | `string` | `"webtop"` | no |
| <a name="input_pers_container_port"></a> [pers\_container\_port](#input\_pers\_container\_port) | The docker container port | `string` | `"80"` | no |
| <a name="input_pers_container_protocol"></a> [pers\_container\_protocol](#input\_pers\_container\_protocol) | The docker container protocol TCP or UDP | `string` | `"TCP"` | no |
| <a name="input_pers_ip_address_type"></a> [pers\_ip\_address\_type](#input\_pers\_ip\_address\_type) | ip address type | `string` | `"public"` | no |
| <a name="input_pers_os_type"></a> [pers\_os\_type](#input\_pers\_os\_type) | OS type where to orchestrate the containers | `string` | `"linux"` | no |
| <a name="input_pers_storage_share_name"></a> [pers\_storage\_share\_name](#input\_pers\_storage\_share\_name) | The Azure storage share name | `string` | `"aci-iac-workflow-share"` | no |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | The prefix used for all resources in this module | `string` | `"prefix"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_fqdn"></a> [fqdn](#output\_fqdn) | the dns fqdn of the container group if dns\_name\_label is set |
| <a name="output_ip_address"></a> [ip\_address](#output\_ip\_address) | the ip address of the container group if dns\_name\_label is set |

<!-- END_TF_DOCS -->
