resource "azurerm_storage_account" "sa_iac_workflow" {
  name                     = "${var.pers_container_name}stor"
  resource_group_name      = var.azurerm_resource_group_name
  location                 = var.azurerm_resource_group_location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_share" "ss_iac_workflow" {
  name                 = var.pers_storage_share_name
  storage_account_name = azurerm_storage_account.sa_iac_workflow.name
  quota                = 50

  depends_on = [azurerm_storage_account.sa_iac_workflow]
}


resource "azurerm_container_group" "aci_iac_workflow" {
  name                = join("-", [var.prefix, var.pers_app_name])
  location            = var.azurerm_resource_group_location
  resource_group_name = var.azurerm_resource_group_name
  ip_address_type     = var.pers_ip_address_type
  dns_name_label      = join("-", [var.prefix, var.pers_app_name])
  os_type             = var.pers_os_type

  container {
    name   = var.pers_container_name
    image  = var.pers_container_image
    cpu    = var.pers_container_cpu
    memory = var.pers_container_memory
    ports {
      port     = var.pers_container_port
      protocol = var.pers_container_protocol
    }
    volume {
      name       = "config"
      mount_path = "/config"
      read_only  = false
      share_name = azurerm_storage_share.ss_iac_workflow.name

      storage_account_name = azurerm_storage_account.sa_iac_workflow.name
      storage_account_key  = azurerm_storage_account.sa_iac_workflow.primary_access_key
    }
  }

  tags = {
    environment = var.environment
  }

  depends_on = [azurerm_storage_share.ss_iac_workflow]
}
